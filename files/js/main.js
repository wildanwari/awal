var services = new Swiper('#swiper-service', {
 effect: 'slide',
 spaceBetween: 50,
 slidesPerView: 2,
 slidesPerGroup: 2,
 autoplay: {
  delay: 1500,
  disableOnInteraction: false,
},
breakpoints: {  

    768: {       
      slidesPerView: 1,
      spaceBetween: 10,
      slidesPerGroup: 1,
    },     

  } ,
pagination: {
  el: '.swiper-pagination',
  clickable: true,
},
});

var testi = new Swiper('#swiper-testi', {
 autoplay: {
  delay: 2000,
  disableOnInteraction: false,
},
pagination: {
  el: '.swiper-pagination',
  clickable: true,
},
});